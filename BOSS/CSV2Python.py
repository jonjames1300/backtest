import csv, os
from prettytable import PrettyTable
from tqdm import tqdm


class Data:
    ###select data and indicies values###
    #print('Please choose:')
    root_directory = os.getcwd()
    os.chdir('Data Files')
    market_type_list = os.listdir()
    for i in range(len(market_type_list)):
        print(i + 1, market_type_list[i])
    market_type_choice = int(input("Please Choose: "))
    market_type_choice = market_type_choice - 1
    os.system('cls')

    os.chdir(market_type_list[market_type_choice])
    curr_direct = os.getcwd()

    instrument_list = os.listdir()
    for i in range(len(instrument_list)):
        print(i + 1, instrument_list[i][:-4])
    instrument_choice = int(input("Please Choose: "))
    instrument_choice = instrument_choice - 1
    os.system('cls')

    with open(curr_direct + '\\' + instrument_list[instrument_choice], 'r') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        extractCSV = []
        start = []
        date = []
        close = []
        high = []
        low = []
        reversedCSV = []

        for data in readCSV:
            for column in readCSV:
                extractCSV.append(column)

        for data in reversed(extractCSV):
            reversedCSV.append(data)

        for data in reversedCSV:
            date.append(data[0])
            if market_type_list[market_type_choice] == 'Currencies' or market_type_list[market_type_choice] == "Cryptocurrency":
                close.append((float(data[1].replace(',', '')))*1000)
                start.append((float(data[2].replace(',', '')))*1000)
                high.append((float(data[3].replace(',', '')))*1000)
                low.append((float(data[4].replace(',', '')))*1000)
            elif market_type_list[market_type_choice] != 'Currencies' or 'Cryptocurrencies':
                close.append(float(data[1].replace(',', '')))
                start.append(float(data[2].replace(',', '')))
                high.append(float(data[3].replace(',', '')))
                low.append(float(data[4].replace(',', '')))


class Indicator:

    def sma(value, exe_choice, exe_method):
        date = []
        sma = []
        start = []
        close = []
        high = []
        low = []
        total = []
        total_date = []

        for i in range(len(Data.date)):
            if i < value - 1:
                continue
            else:
                sma.append(round(sum(Data.close[i - value + 1:i + 1]) / value, 1))
                date.append(Data.date[i])
                start.append(Data.start[i])
                close.append(Data.close[i])
                high.append(Data.high[i])
                low.append(Data.low[i])

        status = []
        for i in range(len(close)):
            if exe_method == 1:
                if close[i] > sma[i]:
                    status.append(True)
                elif close[i] < sma[i]:
                    status.append(False)
                elif close[i] == sma[i]:
                    if i == 0:
                        status.append(None)
                    else:
                        status.append(status[i - 1])
                else:
                    status.append("???")

            elif exe_method == 2:
                if close[i] > sma[i] and start[i] > sma[i]:
                    status.append(True)
                elif close[i] < sma[i] and start[i] < sma[i]:
                    status.append(False)
                elif close[i] == sma[i] and start[i] == sma[i]:
                    if i == 0:
                        status.append(None)
                    else:
                        status.append(status[i - 1])
                elif close[i] > sma[i] or start[i] > sma[i] or close[i] < sma[i] or start[i] < sma[i]:
                    if i == 0:
                        status.append(None)
                    else:
                        status.append(status[i - 1])
                else:
                    status.append("???")

            elif exe_method == 3:
                if close[i] > sma[i] and start[i] > sma[i] and high[i] > sma[i] and low[i] > sma[i]:
                    status.append(True)
                elif close[i] < sma[i] and start[i] < sma[i] and high[i] < sma[i] and low[i] < sma[i]:
                    status.append(False)
                elif close[i] == sma[i] and start[i] == sma[i] and high[i] == sma[i] and low[i] == sma[i]:
                    if i == 0:
                        status.append(None)
                    else:
                        status.append(status[i - 1 ])
                elif close[i] > sma[i] or close[i] < sma[i] or start[i] > sma[i] or start[i] < sma[i] or high[i] > sma[i] or high[i] < sma[i] or low[i] > sma[i] or low[i] < sma[i]:
                    if i == 0:
                        status.append(None)
                    else:
                        status.append(status[i - 1])
                else:
                    status.append("???")

        temp_total = []
        temp_date = []
        temp_status = []
        profit_counter = 0
        for i in range(len(status) - 1):
            if i == 0:
                continue
            elif status[i] != status[i - 1]:
                temp_total.append(start[i + 1])
                temp_date.append(date[i])
                temp_status.append(status[i])
                if len(temp_total) == 2:
                    if temp_status[0] == True:
                        if exe_choice == 1 or exe_choice == 2:
                            total.append(temp_total[1] - temp_total[0])
                            total_date.append(temp_date[0])
                            if (temp_total[1] - temp_total[0]) > 0:
                                profit_counter += 1
                        elif exe_choice == 3:
                            pass
                        else:
                            print('Your code is fucked mate')

                    elif temp_status[0] == False:
                        if exe_choice == 1 or exe_choice == 3:
                            total.append(temp_total[0] - temp_total[1])
                            total_date.append(temp_date[0])
                            if (temp_total[0] - temp_total[1]) > 0:
                                profit_counter += 1
                        elif exe_choice == 2:
                            pass
                        else:
                            print('Your code is fucked mate')

                    temp_date = []
                    temp_total = []
                    temp_status = []
                    temp_total.append(start[i + 1])
                    temp_status.append(status[i])
                    temp_date.append(date[i])

        #size = len(total) + 2
        #for i in range(size):
        #    print(total_date[i], total[i])

        if sum(total) > 0:
            #print(value, round(sum(total), 5), round((profit_counter / len(total) * 100), 5), exe_choice)
            if exe_choice == 1:
                exe_choice = 'Long/Short'
            elif exe_choice == 2:
                exe_choice = 'Long'
            elif exe_choice == 3:
                exe_choice = 'Short'
            else:
                exe_choice = 'Huehuehue'
            t.add_row([value, round(sum(total), 5), int(round((profit_counter / len(total) * 100),0)), exe_choice, exe_method])


t = PrettyTable(['Value','Pip P/L','%Profit', 'Long/Short','Execution Method'])
for a in tqdm(range(9, (500 + 1))):
    for b in range(1, (3 + 1)):
        for c in range(1, (3 + 1)):
            Indicator.sma(a, b, c)

print(t)
input('Tada!!!')
#print(Indicator.sma(120))
