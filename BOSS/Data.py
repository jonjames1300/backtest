import csv
import os
from BOSS.Error import Error


class Data:
    def __init__(self):
        self.source = self.source()

    def source(self):
        def type_selection():
            os.chdir('Data Files')
            type_list = os.listdir()
            print(*[str(i + 1) + '. ' + type_list[i] for i in range(len(type_list))], sep='\n')
            type_choice = type_list[int(input("Please Choose: ")) - 1]
            os.system('clear')
            return type_choice

        def instrument_selection(selection):
            os.chdir(selection)
            instrument_list = os.listdir()
            print(*[str(i + 1) + '. ' + instrument_list[i][:-4] for i in range(len(instrument_list))], sep='\n')
            # for i in range(len(instrument_list)):
            #    print(i + 1, instrument_list[i][:-4])
            instrument_choice = instrument_list[int(input("Please Choose: ")) - 1]
            os.system('clear')
            return instrument_choice

        return instrument_selection(type_selection())

    def extract(self):
        error = Error()
        with open(self.source, 'r') as csv_file:
            read_csv = csv.reader(csv_file, delimiter=',')
            extract_csv = [column for data in read_csv for column in read_csv]
        if extract_csv[0][0][-4:] > extract_csv[-1][0][-4:]:
            extract_csv.reverse()
        elif extract_csv[0][0][-4:] < extract_csv[-1][0][-4:]:
            pass
        else:
            error.exception()
        return extract_csv

    def extract_piece(self, index):
        return self.extract()[index]
