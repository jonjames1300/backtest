import os


class Directory:
    def __init__(self, value):
        self.value = value

    def cd(self):
        return os.chdir(self.value)

    def ls(self):
        return os.listdir(self.value)