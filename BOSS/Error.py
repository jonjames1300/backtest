class Error:
    def __init__(self):
        self.general = "There is an error in the code. Program has been stopped. Please contact Backtest Ltd."

    def exception(self):
        raise ValueError(self.general)
