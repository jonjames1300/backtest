from BOSS.Data import Data
from BOSS.Error import Error


class Indicator:
    def __init__(self, value1, data):
        self.data = data
        self.data_extract = self.data.extract()
        self.close = [float(i[1]) for i in self.data_extract]
        self.open = [float(i[2]) for i in self.data_extract]
        self.high = [float(i[3]) for i in self.data_extract]
        self.low = [float(i[4]) for i in self.data_extract]
        self.value1 = value1

    def backtest(self):
        def sma_calculate(index):
            return round(sum(self.close[index - self.value1 + 1: index + 1]) / self.value1, 4)

        first_status, open_trade, open_status, close_trade = [None] * 4
        total = []
        for i in self.data_extract:
            if self.data_extract.index(i) < self.value1:
                continue
            sma = sma_calculate(self.data_extract.index(i))
            current_status = self.close[self.data_extract.index(i)] > sma
            if first_status is None:
                first_status = current_status
            elif current_status == first_status:
                pass
            elif current_status != first_status and first_status != "Complete":
                first_status = "Complete"
                open_trade = self.open[self.data_extract.index(i) + 1]
                open_status = current_status
            elif (current_status != first_status) and (first_status == "Complete") and (open_trade is not None) and \
                    (current_status != open_status):
                if self.data_extract[self.data_extract.index(i)] == self.data_extract[-1]:
                    continue
                close_trade = self.open[self.data_extract.index(i) + 1]
                if open_status is True:
                    total.append(round(close_trade - open_trade, 4))
                elif open_status is False:
                    total.append(round(open_trade - close_trade, 4))
                else:
                    Error.exception()
                open_trade = close_trade
                close_trade = None
                open_status = current_status
            elif (current_status != first_status) and (first_status == "Complete") and (open_trade is not None) and \
                    (current_status == open_status):
                continue
        return round(sum(total), 4)



