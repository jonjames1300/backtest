from BOSS.Indicator import Indicator
from BOSS.Data import Data


d = Data()

for value in range(1, min(500, len(d.extract()))):
    indi = Indicator(value, d)
    if indi.backtest() > 0:
        print(value, indi.backtest())
