import os
import csv


def error():
    raise ValueError("There is an error in the code. Program has been stopped. Please contact Backtest Ltd.")


class CSVData:

    def __init__(self):
        def type_selection():
            os.chdir('Data Files')
            type_list = os.listdir(os.getcwd())
            for i in range(len(type_list)):
                print(i + 1, type_list[i])
            type_choice = type_list[int(input("Please Choose: ")) - 1]
            os.system('clear')
            return type_choice

        self.type_choice = type_selection()

        def instrument_selection():
            os.chdir(self.type_choice)
            instrument_list = os.listdir(os.getcwd())
            for i in range(len(instrument_list)):
                print(i + 1, instrument_list[i][:-4])
            instrument_choice = instrument_list[int(input("Please Choose: ")) - 1]
            os.system('clear')
            return instrument_choice

        self.instrument_choice = instrument_selection()

    def data_extract(self):
        with open(self.instrument_choice, 'r') as csv_file:
            read_csv = csv.reader(csv_file, delimiter=',')
            extract_csv = []
            for data in read_csv:
                for column in read_csv:
                    extract_csv.append(column)
        if extract_csv[0][0][-4:] > extract_csv[-1][0][-4:]:
            extract_csv.reverse()
        elif extract_csv[0][0][-4:] < extract_csv[-1][0][-4:]:
            pass
        else:
            error()
        return extract_csv


data = CSVData()


class Indicator:
    def __init__(self):
        self.data = data.data_extract()
        # if data.type_choice == 'Currencies' or data.type_choice == 'Cryptocurrency':
        #     self.data = [i.replace(',', '') for i in self.data]

    def calculate(self, indicator_choice, value1):
        pip_container = []

        def sma(value, index, source):
            if index < value:
                return
            elif index >= value:
                data_container = []
                for i in self.data[index-value:index]:
                    data_container.append(float(i[source]))
            return sum(data_container) / value
        previous_sma = None
        status = None
        entry_value = None
        exit_value = None
        for i in self.data:
            sma = sma(value1, self.data.index(i), 1)
            if sma is None and status is None:
                pass
            elif i[1] > sma and previous_sma is None and status is None:
                status = i[1] > sma
                previous_sma = sma
            elif i[1] < sma and previous_sma is None and status is None:
                status = i[1] > sma
                previous_sma = sma
            elif (i[1] > sma) != status:
                if entry_value is None:
                    entry_value = self.data[self.data.index(i) + 1]
                elif entry_value is not None:
                    exit_value = self.data[self.data.index(i) + 1]
                status = i[1] > sma


Indicator().calculate("SMA", None, 10, 0)







