from tqdm import trange
import os
import csv
import mysql.connector


def data_type_list():
    os.chdir('Data Files')
    market_type_list = os.listdir(os.getcwd())
    for i in range(len(market_type_list)):
        print(i + 1, market_type_list[i])
    return market_type_list


data_type_list = data_type_list()


def data_type_choice():
    market_type_choice = int(input("Please Choose: "))
    market_type_choice = market_type_choice - 1
    os.system('clear')
    return data_type_list[market_type_choice]


data_type_choice = data_type_choice()


def data_choice():
    os.chdir(data_type_choice)

    instrument_list = os.listdir(os.getcwd())
    for i in range(len(instrument_list)):
        print(i + 1, instrument_list[i][:-4])
    instrument_choice = int(input("Please Choose: "))
    instrument_choice = instrument_choice - 1
    os.system('clear')
    return instrument_list[instrument_choice]


data_choice = data_choice()


def directory_create():
    curr_direct = os.getcwd()
    return "{}/{}".format(curr_direct, data_choice)


directory_create = directory_create()


def data_extract(choice):
    with open(choice, 'r') as csv_file:
        read_csv = csv.reader(csv_file, delimiter=',')
        extract_csv = []
        for data in read_csv:
            for column in read_csv:
                extract_csv.append(column)
    return extract_csv


data_extract = data_extract(directory_create)


def data_reverse():
    reversed_csv = []
    for data in reversed(data_extract):
        reversed_csv.append(data)
    return reversed_csv


if data_extract[0][0][-4:] > data_extract[-1][0][-4:]:
    data_list = data_reverse()
elif data_extract[0][0][-4:] < data_extract[-1][0][-4:]:
    data_list = data_extract(data_choice())


def data_format(number):  # 0 = date, 1 = close, 2 = start, 3 = high, 4 = low
    global data_list
    format_list = []
    for data in data_list:
        if number == 0:
            format_list.append(data[0])
        elif data_type_choice == 'Currencies' or data_type_choice == 'Cryptocurrency':
            format_list.append((float(data[number].replace(',', '')))*1000)
        elif data_type_choice != 'Currencies' or data_type_choice != 'Cryptocurrency':
            format_list.append(float(data[number].replace(',', '')))
        else:
            print("!!!!!")
    return format_list


source_list = [data_format(0), data_format(1), data_format(2), data_format(3), data_format(4)]

connection = mysql.connector.connect(
        host="192.168.0.19",
        user="backtest",
        passwd="Dr4g0n5!",
        database="backtest"
    )
my_cursor = connection.cursor()

source_string = ["Date", "Close", "Open", "High", "Low"]
exe_choice_string = ["Both", "Long", "Short"]
instrument_name = data_choice[:-4]

query_value = 0
query_insert_value = 0
query_list = []
query_insert = []
query_insert_tuple = []


def sma(value, exe_choice, exe_method, source, offset):
    global query_value
    global query_insert_value
    global query_insert
    global query_list
    global query_insert_tuple
    global indicator_bool
    if value != query_value:
        my_cursor.execute("select InstrumentName, IndicatorValue, Source, Offset, BuySellBoth, Mode from Result where "
                          "InstrumentName = '{}' and IndicatorValue = '{}'".format(instrument_name, value))
        query_list = my_cursor.fetchall()
        query_value = value

    if (instrument_name, value, source_string[source], offset, exe_choice_string[exe_choice], str(exe_method)) in query_list:
        return

    date = []
    sma_value = []
    start = []
    close = []
    high = []
    low = []
    total = []
    total_date = []

    if offset > 0:
        for i in range(offset):
            sma_value.append(None)
    elif offset == 0:
        pass
    else:
        print('Problem')

    for i in range(len(source_list[0])):
        if i < value - 1:
            pass
        else:
            sma_value.append(round(sum(source_list[source][i - value + 1:i + 1]) / value, 1))
            date.append(source_list[0][i])
            start.append(source_list[2][i])
            close.append(source_list[1][i])
            high.append(source_list[3][i])
            low.append(source_list[4][i])

    sma_value = sma_value[offset:]
    date = date[offset:]
    start = start[offset:]
    close = close[offset:]
    high = high[offset:]
    low = low[offset:]

    status = []
    for i in range(len(close)):
        if sma_value[i] is None:
            pass
        elif exe_method == 1:
            if close[i] > sma_value[i]:
                status.append(True)
            elif close[i] < sma_value[i]:
                status.append(False)
            elif close[i] == sma_value[i]:
                if i == 0 or sma_value[i-1] is None:
                    status.append(None)
                else:
                    status.append(status[i - 1])
            else:
                status.append("???")

        elif exe_method == 2:
            if close[i] > sma_value[i] and start[i] > sma_value[i]:
                status.append(True)
            elif close[i] < sma_value[i] and start[i] < sma_value[i]:
                status.append(False)
            elif close[i] == sma_value[i] and start[i] == sma_value[i]:
                if i == 0 or sma_value[i-1] is None:
                    status.append(None)
                else:
                    status.append(status[i - 1])
            elif close[i] > sma_value[i] or start[i] > sma_value[i] or \
                    close[i] < sma_value[i] or start[i] < sma_value[i]:
                if i == 0 or sma_value[i-1] is None:
                    status.append(None)
                else:
                    status.append(status[i - 1])
            else:
                status.append("???")

        elif exe_method == 3:
            if close[i] > sma_value[i] and start[i] > sma_value[i] and \
                    high[i] > sma_value[i] and low[i] > sma_value[i]:
                status.append(True)
            elif close[i] < sma_value[i] and start[i] < sma_value[i] and \
                    high[i] < sma_value[i] and low[i] < sma_value[i]:
                status.append(False)
            elif close[i] == sma_value[i] and start[i] == sma_value[i] and \
                    high[i] == sma_value[i] and low[i] == sma_value[i]:
                if i == 0 or sma_value[i-1] is None:
                    status.append(None)
                else:
                    status.append(status[i - 1])
            elif close[i] > sma_value[i] or close[i] < sma_value[i] or \
                    start[i] > sma_value[i] or start[i] < sma_value[i] or \
                    high[i] > sma_value[i] or high[i] < sma_value[i] or \
                    low[i] > sma_value[i] or low[i] < sma_value[i]:
                if i == 0 or sma_value[i-1] is None:
                    status.append(None)
                else:
                    status.append(status[i - 1])
            else:
                status.append("???")

    temp_total = []
    temp_date = []
    temp_status = []
    profit_counter = 0
    for i in range(len(status) - 1):
        if i == 0:
            continue
        elif status[i] != status[i - 1]:
            temp_total.append(start[i + 1])
            temp_date.append(date[i])
            temp_status.append(status[i])
            if len(temp_total) == 2:
                if temp_status[0] == True:
                    if exe_choice == 0 or exe_choice == 1:
                        total.append(temp_total[1] - temp_total[0])
                        total_date.append(temp_date[0])
                        if (temp_total[1] - temp_total[0]) > 0:
                            profit_counter += 1
                    elif exe_choice == 2:
                        pass
                    else:
                        print('Your code is fucked mate')

                elif temp_status[0] == False:
                    if exe_choice == 0 or exe_choice == 2:
                        total.append(temp_total[0] - temp_total[1])
                        total_date.append(temp_date[0])
                        if (temp_total[0] - temp_total[1]) > 0:
                            profit_counter += 1
                    elif exe_choice == 1:
                        pass
                    else:
                        print('Your code is fucked mate')

                temp_date = []
                temp_total = []
                temp_status = []
                temp_total.append(start[i + 1])
                temp_status.append(status[i])
                temp_date.append(date[i])

    try:
        percentage = profit_counter / len(total) * 100
    except ZeroDivisionError:
        percentage = 0

    if query_insert_value == value:
        query_insert.append([instrument_name, value, source_string[source], offset, exe_choice_string[exe_choice],
                             exe_method, sum(total), percentage])
        return
    elif query_insert_value != value:
        for i in query_insert:
            query_insert_tuple.append(tuple(i))
        sql = """INSERT INTO Result (InstrumentName, IndicatorValue, 
        Source, Offset, BuySellBoth, Mode, Pip, Percentage) 
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""
        val = query_insert_tuple
        my_cursor.executemany(sql, val)
        connection.commit()
        query_insert = []
        query_insert_tuple = []
        query_insert_value = value
        query_insert.append([instrument_name, value, source_string[source], offset, exe_choice_string[exe_choice],
                             exe_method, sum(total), percentage])


for a in trange(9, int(min((500 + 1), len(source_list[0])))):
    for b in trange(3):
        for c in trange(1, (3 + 1)):
            for d in trange(1, len(source_list)):
                for e in trange(250 + 1):
                    if (a + e) >= len(source_list[0]):
                        continue
                    else:
                        sma(a, b, c, d, e)


# print('')

input('Complete!!!')
