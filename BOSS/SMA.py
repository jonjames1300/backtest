from BOSS.Indicator import Indicator
from BOSS.Data import Data


class SMA(Indicator):
    def __init__(self, data, value1):
        Indicator.__init__(self, value1)
        self.sma_value = sum(data[index - value1: index]) / value1
        self.sma_status = data > self.sma_value


def start_backtest(value):
    data = Data()
    data.extract()
    sma = SMA(data.extract(), value)
    sma.backtest()
    return

start_backtest(9)