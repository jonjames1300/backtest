import os #comments for jons sake
print(os.getcwd())
default = "E:\\Market Data\\"
os.chdir(default)
print(os.getcwd())
indicator = ['None', 'Simple Moving Average']

invest = ['Equity', 'CFD']
atype = ['Candle', 'Heikin Ashi', 'Baseline', 'Renko', 'Line Break', 'Kagi', 'Point + Figure']
time = ['1 Minute', '5 Minute', '15 Minute', '30 Minute', '45 Minute', '1 Hour', '2 Hour', '4 Hour', '5 Hour', '1 Day', '1 Week', '1 Month']
indicator1 = indicator
indValue1 = range(1, 500+1)
indicator2 = indicator
indValue2 = range(1, 500+1)
indicator3 = indicator
indValue3 = range(1, 500+1)
indicator4 = indicator
indValue4 = range(1, 500+1)
stopLoss = ['None', 'Stop Loss', 'Trailing Stop']
stopValue = range(1, 10000+1)
takeProfit = ['None', 'Take Profit']
action = ['Long', 'Short', 'Both']

#for each graph type
for a in atype:
    os.chdir(default)
    os.mkdir(a)

    #for each time 
    for b in time:
        os.chdir(default + a + "\\")
        os.mkdir(b)

        #for each indicator
        for c in indicator:
            os.chdir(default + a + "\\" + b + "\\")
            os.mkdir(c)

            #loops code 500 times?
            #For each possible value of the indicator
            for d in indValue:
                os.chdir(default + a + "\\" + b + "\\" + c + "\\")
                os.mkdir(str(d))
#i assume this code creates a folder for every single graph type youve included, creating subfolders for each time, and more subfolders for each indictaors, looping 500ish times?

#codes looking good dude
